/**
 * @author Edouard DUPIN
 * 
 * @copyright 2014, Edouard DUPIN, all right reserved
 * 
 * @license APACHE-2 (see license file)
 */

#ifndef __ECI_CLASS_H__
#define __ECI_CLASS_H__

#include <etk/types.h>

namespace eci {
	class Class {
		public:
			Class() {};
			~Class() {};
	};
}

#endif
